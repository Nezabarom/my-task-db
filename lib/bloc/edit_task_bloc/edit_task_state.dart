import 'package:equatable/equatable.dart';

class EditTaskState extends Equatable {

  @override
  List<Object> get props => [];
}

class EditTaskInitialState extends EditTaskState {}

class EditTaskTapState extends EditTaskState {}

class EditTaskPickDateState extends EditTaskState {
  EditTaskPickDateState({required this.dateTimeT});

  final int dateTimeT;

  @override
  List<Object> get props => [dateTimeT];
}

class EditTaskPickTimeState extends EditTaskState {
  EditTaskPickTimeState({required this.timeTimeT});

  final int timeTimeT;

  @override
  List<Object> get props => [timeTimeT];
}

class EditTaskPrioritySelectorState extends EditTaskState {
  EditTaskPrioritySelectorState({required this.prioritySelectorList});
  final List<bool> prioritySelectorList;

  @override
  List<Object> get props => [prioritySelectorList];
}