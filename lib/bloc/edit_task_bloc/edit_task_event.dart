// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class EditTaskEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class EditTaskInitEvent extends EditTaskEvent{
  EditTaskInitEvent({required this.timeTimeT, required this.dateTimeT, required this.priority});

  final int timeTimeT;
  final int dateTimeT;
  final String priority;

  @override
  List<Object> get props => [timeTimeT, dateTimeT, priority];
}

class EditTaskTapEvent extends EditTaskEvent {
  EditTaskTapEvent(this.taskModel, this.prioritySelectorList);

  final TaskModel taskModel;
  final List<bool> prioritySelectorList;

  @override
  List<Object> get props => [taskModel, prioritySelectorList];
}

class EditTaskPrioritySelectorEvent extends EditTaskEvent {
  EditTaskPrioritySelectorEvent(this.selectorIndex, this.priorityList);

  final int selectorIndex;
  List<bool> priorityList;

  @override
  List<Object> get props => [selectorIndex, priorityList];
}
class EditTaskInitPriorityEvent extends EditTaskEvent {
  EditTaskInitPriorityEvent(this.priority);

  final String priority;

  @override
  List<Object> get props => [priority];
}

class EditTaskPickDateEvent extends EditTaskEvent {
  EditTaskPickDateEvent(this.data);

  final DateTime data;

  @override
  List<Object> get props => [data];
}

class EditTaskPickTimeEvent extends EditTaskEvent {
  EditTaskPickTimeEvent(this.time);

  final TimeOfDay time;

  @override
  List<Object> get props => [time];
}