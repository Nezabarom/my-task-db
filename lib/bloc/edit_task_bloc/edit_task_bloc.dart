import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/edit_task_bloc/edit_task_event.dart';
import 'package:my_task_db/bloc/edit_task_bloc/edit_task_state.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class EditTaskBloc extends Bloc<EditTaskEvent, EditTaskState> {
  EditTaskBloc({required TaskRepository taskRepository})
      : _taskRepository = taskRepository,
        super(EditTaskInitialState()) {
    on<EditTaskInitEvent>(init);
    on<EditTaskTapEvent>(editTaskTap);
    on<EditTaskPickDateEvent>(pickDate);
    on<EditTaskPickTimeEvent>(pickTime);
    on<EditTaskPrioritySelectorEvent>(prioritySelector);
  }

  final SharedPreference _sharedPreference = SharedPreferenceImpl();
  final TaskRepository _taskRepository;

  init(EditTaskInitEvent event, Emitter<EditTaskState> emit) {
    emit(EditTaskPickTimeState(timeTimeT: event.timeTimeT));
    emit(EditTaskPickDateState(dateTimeT: event.dateTimeT));
    late List<bool> priorityList;
    if (event.priority == 'Low') {
      priorityList = [true, false, false];
    } else if (event.priority == 'Normal') {
      priorityList = [false, true, false];
    } else if (event.priority == 'High') {
      priorityList = [false, false, true];
    }
    emit(EditTaskPrioritySelectorState(prioritySelectorList: priorityList));
    emit(EditTaskInitialState());
  }

  pickDate(EditTaskPickDateEvent event, Emitter<EditTaskState> emit) {
    DateTime startEpochDAte = DateTime(1970, 1, 1);
    Duration? difference = event.data.difference(startEpochDAte);
    int dateTimeT = difference.inSeconds;
    emit(EditTaskPickDateState(dateTimeT: dateTimeT));
    emit(EditTaskInitialState());
  }

  pickTime(EditTaskPickTimeEvent event, Emitter<EditTaskState> emit) {
    Duration difference =
        Duration(hours: event.time.hour, minutes: event.time.minute);
    emit(EditTaskPickTimeState(timeTimeT: difference.inSeconds));
    emit(EditTaskInitialState());
  }

  editTaskTap(EditTaskTapEvent event, Emitter<EditTaskState> emit) async {
    String userId = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    await _taskRepository.editTask(userId, TaskModel(
        intId: event.taskModel.intId,
        stringId: event.taskModel.stringId,
        title: event.taskModel.title,
        dueBy: event.taskModel.dueBy,
        priority: event.taskModel.priority));
    emit(EditTaskTapState());
  }

  prioritySelector(
      EditTaskPrioritySelectorEvent event, Emitter<EditTaskState> emit) {
    if (event.selectorIndex == 0) {
      event.priorityList = [true, false, false];
    } else if (event.selectorIndex == 1) {
      event.priorityList = [false, true, false];
    } else if (event.selectorIndex == 2) {
      event.priorityList = [false, false, true];
    }
    emit(EditTaskPrioritySelectorState(
        prioritySelectorList: event.priorityList));
  }
}
