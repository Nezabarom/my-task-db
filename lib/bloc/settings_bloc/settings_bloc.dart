import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/settings_bloc/settings_event.dart';
import 'package:my_task_db/bloc/settings_bloc/settings_state.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingState> {
  SettingsBloc() : super(InitSettingsState()) {
    on<SortingDropDownValueEvent>(changeSortDropDownValue);
    on<GetDefaultSortValueEvent>(getDefaultDropDownValue);
    on<LogOutEvent>(logOut);
  }

  final SharedPreference _sharedPreference = SharedPreferenceImpl();

  changeSortDropDownValue(SortingDropDownValueEvent event,
      Emitter<SettingState> emit) {
    _sharedPreference.removePreferenceValue(
        SharedPreferenceImpl.defaultSortType);
    _sharedPreference.setStringPreferenceValue(
        SharedPreferenceImpl.defaultSortType, event.newSortValue);
    emit(SortingDropDownValueState(event.newSortValue));
    emit(GetDefaultSortValueState(event.newSortValue));
  }

  Future getDefaultDropDownValue(GetDefaultSortValueEvent event,
      Emitter<SettingState> emit) async {
    await _sharedPreference.getStringPreferenceValue(
        SharedPreferenceImpl.defaultSortType).then((value) {
      emit(GetDefaultSortValueState(value));
    });
  }

  Future logOut(LogOutEvent event,
      Emitter<SettingState> emit) async {
    await _sharedPreference
        .removePreferenceValue(SharedPreferenceImpl.token);
    await _sharedPreference
        .removePreferenceValue(SharedPreferenceImpl.defaultSortType);
    await _sharedPreference.setBoolPreferenceValue(
        SharedPreferenceImpl.isUserExist, false);
    String defaultSortType = await _sharedPreference.getStringPreferenceValue(
        SharedPreferenceImpl.defaultSortType);
    emit(LogOutState());
    emit(GetDefaultSortValueState(defaultSortType));
  }
}