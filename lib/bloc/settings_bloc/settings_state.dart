import 'package:equatable/equatable.dart';

class SettingState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitSettingsState extends SettingState{}

class SortingDropDownValueState extends SettingState {
  SortingDropDownValueState(this.sortingDropdownValue);

  final String sortingDropdownValue;
  @override
  List<Object> get props => [sortingDropdownValue];
}

class GetDefaultSortValueState extends SettingState {
  GetDefaultSortValueState(this.defaultSortValue);

  final String defaultSortValue;
  @override
  List<Object> get props => [defaultSortValue];
}

class LogOutState extends SettingState {
  @override
  List<Object> get props => [];
}
