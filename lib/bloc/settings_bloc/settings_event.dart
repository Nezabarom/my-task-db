import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class SettingsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SortingDropDownValueEvent extends SettingsEvent {
  SortingDropDownValueEvent(this.newSortValue);
  final String newSortValue;

  @override
  List<Object> get props => [newSortValue];
}

class GetDefaultSortValueEvent extends SettingsEvent {
  GetDefaultSortValueEvent(this.context);
  final BuildContext context;

  @override
  List<Object> get props => [context];
}

class LanguageDropDownValueEvent extends SettingsEvent {
  LanguageDropDownValueEvent(this.newLanguageValue, this.context);
  final String newLanguageValue;
  final BuildContext context;

  @override
  List<Object> get props => [newLanguageValue, context];
}

class LogOutEvent extends SettingsEvent {
  @override
  List<Object> get props => [];
}
