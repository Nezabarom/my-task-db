import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/create_task_bloc/create_task_event.dart';
import 'package:my_task_db/bloc/create_task_bloc/create_task_state.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';
import 'package:my_task_db/models/tasks/task_model.dart';
import 'package:my_task_db/utils/toasts_message.dart';

class CreateTaskBloc extends Bloc<CreateTaskEvent, CreateTaskState> {
  CreateTaskBloc({required TaskRepository taskRepository})
      : _taskRepository = taskRepository,
        super(CreateTaskInitialState()) {
    on<CreateTaskInitEvent>(init);
    on<CreateTaskPickDateEvent>(pickDate);
    on<CreateTaskPickTimeEvent>(pickTime);
    on<CreateTaskPrioritySelectorEvent>(prioritySelector);
    on<CreateTaskTapEvent>(createTaskTap);
  }

  final TaskRepository _taskRepository;
  final SharedPreference _sharedPreference = SharedPreferenceImpl();

  init(CreateTaskInitEvent event, Emitter<CreateTaskState> emit) {
    emit(CreateTaskPickTimeState(timeTimeT: null));
    emit(CreateTaskPickDateState(dateTimeT: null));
    emit(CreateTaskPrioritySelectorState(
        prioritySelectorList: const [false, false, false]));
    emit(CreateTaskInitialState());
  }

  pickDate(CreateTaskPickDateEvent event, Emitter<CreateTaskState> emit) {
    DateTime startEpochDAte = DateTime(1970, 1, 1);
    Duration? difference = event.data.difference(startEpochDAte);
    int dateTimeT = difference.inSeconds;
    emit(CreateTaskPickDateState(dateTimeT: dateTimeT));
    emit(CreateTaskInitialState());
  }

  pickTime(CreateTaskPickTimeEvent event, Emitter<CreateTaskState> emit) {
    Duration difference =
        Duration(hours: event.time.hour, minutes: event.time.minute);
    emit(CreateTaskPickTimeState(timeTimeT: difference.inSeconds));
    emit(CreateTaskInitialState());
  }

  prioritySelector(
      CreateTaskPrioritySelectorEvent event, Emitter<CreateTaskState> emit) {
    if (event.selectorIndex == 0) {
      event.priorityList = [true, false, false];
    } else if (event.selectorIndex == 1) {
      event.priorityList = [false, true, false];
    } else if (event.selectorIndex == 2) {
      event.priorityList = [false, false, true];
    }
    emit(CreateTaskPrioritySelectorState(
        prioritySelectorList: event.priorityList));
  }

  Future createTaskTap(
      CreateTaskTapEvent event, Emitter<CreateTaskState> emit) async {
    String title = event.titleController;
    int? timeT = changeDateAndTimeToTimeT(event.pickedDate, event.pickedTime);
    List<bool> priorityList = event.priorityList;
    int taskIntId = 1;
    List<String> tasksIdList = await getTasksIdList();
    while (tasksIdList.contains(taskIntId.toString())) {
      taskIntId++;
    }
    tasksIdList.add(taskIntId.toString());
    _sharedPreference.setListStringsPreferenceValue(
        SharedPreferenceImpl.tasksIdList, tasksIdList);
    String userId = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    if (priorityList != [false, false, false]) {
      if (title.length >= 5) {
        if (timeT != null) {
          String taskPriority = changePriorityListToPriority(priorityList);
          await _taskRepository.createTask(userId, TaskModel(
              intId: taskIntId.toString(),
              stringId: '',
              title: title,
              dueBy: timeT,
              priority: taskPriority));
          emit(CreateTaskTapState());
        } else {
          MessageUtils.toast(
              message: UiStrings.pickDateAndTime, type: MessageType.warning);
        }
      } else {
        MessageUtils.toast(
            message: UiStrings.pickTitleDescription, type: MessageType.warning);
      }
    } else {
      MessageUtils.toast(
          message: UiStrings.pickPriority, type: MessageType.warning);
    }
  }

  int? changeDateAndTimeToTimeT(DateTime? pickedDate, TimeOfDay? pickedTime) {
    if (pickedDate != null && pickedTime != null) {
      DateTime startEpochDAte = DateTime(1970, 1, 1);
      Duration? differenceDate = pickedDate.difference(startEpochDAte);
      Duration differenceTime =
          Duration(hours: pickedTime.hour, minutes: pickedTime.minute);
      int timeT = differenceDate.inSeconds + differenceTime.inSeconds;
      return timeT;
    } else {
      return null;
    }
  }

  String changePriorityListToPriority(List<bool> priorityList) {
    late String taskPriority;
    if (priorityList[0] == true) {
      taskPriority = 'Low';
    } else if (priorityList[1] == true) {
      taskPriority = 'Normal';
    } else if (priorityList[2] == true) {
      taskPriority = 'High';
    }
    return taskPriority;
  }

  Future<List<String>> getTasksIdList() async {
    String userId = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    List<TaskModel>? tasksList = await _taskRepository.getTasksList(userId);
    List<String> tasksIdList = [];
    if (tasksList != null) {
      for (var element in tasksList) {
        String idTask = element.intId.toString();
        tasksIdList.add(idTask);
      }
    }
    return tasksIdList;
  }
}
