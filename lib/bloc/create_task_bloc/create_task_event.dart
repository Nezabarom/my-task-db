import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class CreateTaskEvent extends Equatable {}

class CreateTaskInitEvent extends CreateTaskEvent{
  @override
  List<Object> get props => [];
}

class CreateTaskTapEvent extends CreateTaskEvent {
  CreateTaskTapEvent(
      {required this.titleController,
      required this.pickedDate,
      required this.pickedTime,
      required this.priorityList});

  final String titleController;
  final DateTime? pickedDate;
  final TimeOfDay? pickedTime;
  final List<bool> priorityList;

  @override
  List<Object?> get props => [titleController, pickedDate,pickedTime,  priorityList];
}

// ignore: must_be_immutable
class CreateTaskPrioritySelectorEvent extends CreateTaskEvent {
  CreateTaskPrioritySelectorEvent(this.selectorIndex, this.priorityList);

  final int selectorIndex;
  List<bool> priorityList;

  @override
  List<Object> get props => [selectorIndex, priorityList];
}

class CreateTaskPickDateEvent extends CreateTaskEvent {
  CreateTaskPickDateEvent(this.data);

  final DateTime data;

  @override
  List<Object> get props => [data];
}

class CreateTaskPickTimeEvent extends CreateTaskEvent {
  CreateTaskPickTimeEvent(this.time);

  final TimeOfDay time;

  @override
  List<Object> get props => [time];
}
