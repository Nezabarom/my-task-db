import 'package:equatable/equatable.dart';

abstract class CreateTaskState extends Equatable {
}

class CreateTaskPrioritySelectorState extends CreateTaskState {
  CreateTaskPrioritySelectorState({required this.prioritySelectorList});

  final List<bool> prioritySelectorList;

  @override
  List<Object> get props => [prioritySelectorList];
}

class CreateTaskPickDateState extends CreateTaskState {
  CreateTaskPickDateState({required this.dateTimeT});

  final int? dateTimeT;

  @override
  List<Object?> get props => [dateTimeT];
}

class CreateTaskPickTimeState extends CreateTaskState {
  CreateTaskPickTimeState({required this.timeTimeT});

  final int? timeTimeT;

  @override
  List<Object?> get props => [timeTimeT];
}

class CreateTaskTapState extends CreateTaskState {
  @override
  List<Object> get props => [];
}

class CreateTaskInitialState extends CreateTaskState {
  @override
  List<Object> get props => [];
}
