import 'package:equatable/equatable.dart';

class HomeEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class NavBarTapHomeEvent extends HomeEvent {
  NavBarTapHomeEvent(this.selectedIndex);

  final int selectedIndex;

  @override
  List<Object> get props => [selectedIndex];
}