import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/home_bloc/home_event.dart';
import 'package:my_task_db/bloc/home_bloc/home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(NavBarTapHomeState(0)) {
    on<NavBarTapHomeEvent>(changeIndex);
  }

  changeIndex(NavBarTapHomeEvent event,
      Emitter<HomeState> emit){
    int newIndex = event.selectedIndex;
    emit(NavBarTapHomeState(newIndex));
  }
}