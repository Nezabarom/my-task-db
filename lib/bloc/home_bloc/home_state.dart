import 'package:equatable/equatable.dart';

class HomeState extends Equatable {

  @override
  List<Object> get props => [];
}

class NavBarTapHomeState extends HomeState {
  NavBarTapHomeState(this.selectedIndex);

  final int selectedIndex;

  @override
  List<Object> get props => [selectedIndex];
}