import 'package:equatable/equatable.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class NotificationListEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class InitTaskListEvent extends NotificationListEvent {
}

class GetNotificationListEvent extends NotificationListEvent {
}

class RemoveNotificationListEvent extends NotificationListEvent {

  final List<TaskModel> notificationList;
  RemoveNotificationListEvent(this.notificationList);

  @override
  List<Object> get props => [notificationList];
}

class RemoveNotificationByIdEvent extends NotificationListEvent {

  final List<TaskModel> notificationList;
  final int index;
  final String id;

  RemoveNotificationByIdEvent(this.id, this.index, this.notificationList);

  @override
  List<Object> get props => [id, index, notificationList];
}
