import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_event.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_state.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';
import 'package:my_task_db/models/tasks/task_model.dart';
import 'package:my_task_db/utils/local_notification.dart';

class NotificationListBloc
    extends Bloc<NotificationListEvent, NotificationListState> {
  NotificationListBloc({required TaskRepository taskRepository})
      : _taskRepository = taskRepository,
        super(InitNotificationListState()) {
    on<GetNotificationListEvent>(getListOfNotification);
    on<RemoveNotificationByIdEvent>(removeNotificationById);
    on<RemoveNotificationListEvent>(removeListNotification);
  }

  final SharedPreference _sharedPreference = SharedPreferenceImpl();
  final TaskRepository _taskRepository;

  Future getListOfNotification(GetNotificationListEvent event,
      Emitter<NotificationListState> emit) async {
    List<String> notificationIdList = await _sharedPreference
        .getListStringsPreferenceValue(SharedPreferenceImpl.notificationList);
    String userId = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    List<TaskModel>? taskList;
    List<TaskModel> notificationList;
    await _taskRepository.getTasksList(userId).then((value) {
      taskList = value;
    });
    notificationList = [];
    for (var element in taskList ?? []) {
      if (notificationIdList.contains(element.intId.toString())) {
        notificationList.add(element);
      }
    }
    emit(GetNotificationListState(notificationList));
  }

  Future removeNotificationById(RemoveNotificationByIdEvent event,
      Emitter<NotificationListState> emit) async {
    List<String> notificationIdList = await _sharedPreference
        .getListStringsPreferenceValue(SharedPreferenceImpl.notificationList);
    notificationIdList.remove(event.id);
    _sharedPreference
        .removePreferenceValue(SharedPreferenceImpl.notificationList)
        .then((value) {
      _sharedPreference.setListStringsPreferenceValue(
          SharedPreferenceImpl.notificationList, notificationIdList);
      int notificationIdInt = int.parse(event.id);
      NotificationService().removeNotification(notificationIdInt);
    });
    event.notificationList.removeAt(event.index);
    emit(RemoveNotificationByIdState());
    emit(GetNotificationListState(event.notificationList));
  }

  removeListNotification(
      RemoveNotificationListEvent event, Emitter<NotificationListState> emit) {
    _sharedPreference
        .removePreferenceValue(SharedPreferenceImpl.notificationList);
    NotificationService().removeAllNotification();
    event.notificationList.clear();
    emit(RemoveNotificationListState());
    emit(GetNotificationListState(event.notificationList));
  }
}
