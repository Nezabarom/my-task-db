import 'package:equatable/equatable.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class NotificationListState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitNotificationListState extends NotificationListState {}

class GetNotificationListState extends NotificationListState {
  final List<TaskModel> notificationList;
  GetNotificationListState(this.notificationList);

  @override
  List<Object> get props => [notificationList];
}

class RemoveNotificationByIdState extends NotificationListState {
}

class RemoveNotificationListState extends NotificationListState {
}