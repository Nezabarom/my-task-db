import 'package:equatable/equatable.dart';
import 'package:my_task_db/enums/sort_type_enum.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class TaskListState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitTaskListState extends TaskListState {}

class GetTaskListState extends TaskListState {
  final SortType sortType;
  final List<TaskModel> taskList;

  GetTaskListState(this.taskList, this.sortType);

  @override
  List<Object> get props => [taskList, sortType];
}
