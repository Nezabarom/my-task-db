import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_event.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_state.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';
import 'package:my_task_db/enums/sort_type_enum.dart';
import 'package:my_task_db/models/tasks/task_model.dart';
import 'package:my_task_db/utils/sorting_task_list.dart';

class TaskListBloc extends Bloc<TaskListEvent, TaskListState> {
  TaskListBloc({required TaskRepository taskRepository})
      : _taskRepository = taskRepository,
        super(InitTaskListState()) {
    on<GetTaskListEvent>(_getListOfTasks);
    on<SortedListEvent>(_getSortListOfTasks);
  }

  final TaskRepository _taskRepository;
  final SharedPreference _sharedPreference = SharedPreferenceImpl();

  Future _getListOfTasks(
      GetTaskListEvent event, Emitter<TaskListState> emit) async {
    emit(InitTaskListState());
    SortType sortType = await _sharedPreference
        .getDefaultSortType(SharedPreferenceImpl.defaultSortType);
    String userId = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    await _taskRepository.getTasksList(userId).then((value) async {
      List<TaskModel> _taskList;
      await Future.delayed(const Duration(milliseconds: 500));
      if (value == null) {
        _taskList = [];
      } else {
        _taskList = value;
      }
      List<TaskModel> sortedTaskList =
          SortingTaskList.sort(_taskList, sortType);
      emit(GetTaskListState(sortedTaskList, sortType));
    });
  }

  _getSortListOfTasks(SortedListEvent event, Emitter<TaskListState> emit) {
    List<TaskModel> sortedTaskList =
        SortingTaskList.sort(event.taskList, event.sortType);
    emit(GetTaskListState(sortedTaskList, event.sortType));
  }
}
