import 'package:equatable/equatable.dart';
import 'package:my_task_db/enums/sort_type_enum.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

abstract class TaskListEvent extends Equatable {}

class InitTaskListEvent extends TaskListEvent {
  @override
  List<Object> get props => [];
}

class GetTaskListEvent extends TaskListEvent {

  @override
  List<Object> get props => [];
}

class SortedListEvent extends TaskListEvent {

  final SortType sortType;
  final List<TaskModel> taskList;
  SortedListEvent(this.sortType, this.taskList);

  @override
  List<Object> get props => [sortType, taskList];
}