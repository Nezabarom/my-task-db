import 'package:equatable/equatable.dart';

class AuthState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitAuthState extends AuthState {}

class LoginState extends AuthState {
  final bool isLogin;
  LoginState(this.isLogin);

  @override
  List<Object> get props => [isLogin];
}

class RegistrationState extends AuthState {
  final bool isRegistration;
  RegistrationState(this.isRegistration);

  @override
  List<Object> get props => [isRegistration];
}

class ChangeButtonState extends AuthState {
  final bool isRegistered;
  ChangeButtonState(this.isRegistered);

  @override
  List<Object> get props => [isRegistered];
}