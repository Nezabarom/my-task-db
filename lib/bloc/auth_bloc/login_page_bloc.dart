import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/auth_bloc/login_page_event.dart';
import 'package:my_task_db/bloc/auth_bloc/login_page_state.dart';
import 'package:my_task_db/data/repository/network/auth/auth_repository_impl.dart';
import 'package:my_task_db/models/auth/user_model.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc({required AuthRepository authRepository})
      : _authRepository = authRepository,
        super(ChangeButtonState(true)) {
    on<LoginEvent>(_logIn);
    on<ChangeButtonEvent>(_changeButton);
    on<RegistrationEvent>(_registration);
  }

  final AuthRepository _authRepository;

  Future _logIn(LoginEvent event, Emitter<AuthState> emit) async {
    bool isValidation =
        await _validationLogInAndPassword(event.login, event.password);
    if (isValidation) {
      bool isLogin = await _authRepository.logIn(event.login, event.password);
      if (isLogin) {
        emit(LoginState(isLogin));
        emit(ChangeButtonState(true));
      }
    }
  }

  Future _changeButton(ChangeButtonEvent event, Emitter<AuthState> emit) async {
    bool isRegistered = !event.isRegistered;
    emit(ChangeButtonState(isRegistered));
  }

  Future _registration(RegistrationEvent event, Emitter<AuthState> emit) async {
    bool isValidation =
        await _validationLogInAndPassword(event.login, event.password);
    if (isValidation) {
      bool isLogInSuccessful =
          await _authRepository.registration(event.login, event.password);
      if (isLogInSuccessful) {
        User? user = FirebaseAuth.instance.currentUser;
        _authRepository.addUser(UserModel(
            id: user?.uid, email: event.login, password: event.password));
        emit(RegistrationState(isLogInSuccessful));
        emit(ChangeButtonState(true));
      }
    }
  }

  Future<bool> _validationLogInAndPassword(
      String email, String password) async {
    bool isValidation = true;
    if (email.contains(RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
      return isValidation;
    }
    if (email.isEmpty || password.isEmpty) {
      isValidation == false;
    }
    return isValidation;
  }
}
