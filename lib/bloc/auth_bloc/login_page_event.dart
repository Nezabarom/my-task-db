import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {}

class LoginEvent extends AuthEvent {
  final String login;
  final String password;

  LoginEvent(this.login, this.password);

  @override
  List<Object> get props => [login, password];
}

class RegistrationEvent extends AuthEvent {
  final String login;
  final String password;

  RegistrationEvent(this.login, this.password);

  @override
  List<Object> get props => [login, password];
}

class ChangeButtonEvent extends AuthEvent {

  final bool isRegistered;
  ChangeButtonEvent(this.isRegistered);

  @override
  List<Object> get props => [isRegistered];
}
