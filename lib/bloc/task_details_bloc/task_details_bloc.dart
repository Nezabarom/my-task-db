import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/task_details_bloc/task_details_event.dart';
import 'package:my_task_db/bloc/task_details_bloc/task_details_state.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';
import 'package:my_task_db/utils/local_notification.dart';
import 'package:my_task_db/utils/toasts_message.dart';

class TaskDetailsBloc extends Bloc<TaskDetailsEvent, TaskDetailsState> {
  TaskDetailsBloc({required TaskRepository taskRepository})
      : _taskRepository = taskRepository,
        super(InitTaskDetailsState(indexMinute: 2)) {
    on<DeleteTaskEvent>(deleteTask);
    on<IncrementMinuteEvent>(incrementMinute);
    on<DecrementMinuteEvent>(decrementMinute);
    on<AddNotificationEvent>(addNotification);
  }

  final TaskRepository _taskRepository;
  final SharedPreference _sharedPreference = SharedPreferenceImpl();

  void deleteTask(DeleteTaskEvent event, Emitter<TaskDetailsState> emit) async {
    List<String> notificationIdList = await _sharedPreference
        .getListStringsPreferenceValue(SharedPreferenceImpl.notificationList);
    String userId = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    _taskRepository.deleteTask(event.taskModel.stringId!, userId);
    notificationIdList.remove(event.taskModel.intId.toString());
    _sharedPreference
        .removePreferenceValue(SharedPreferenceImpl.notificationList)
        .then((value) {
      _sharedPreference.setListStringsPreferenceValue(
          SharedPreferenceImpl.notificationList, notificationIdList);
    });
  }

  void incrementMinute(
      IncrementMinuteEvent event, Emitter<TaskDetailsState> emit) {
    if (event.indexMinute < 5) {
      event.indexMinute++;
      emit(InitTaskDetailsState(indexMinute: event.indexMinute));
    }
  }

  void decrementMinute(
      DecrementMinuteEvent event, Emitter<TaskDetailsState> emit) {
    if (event.indexMinute > 0) {
      event.indexMinute--;
      emit(InitTaskDetailsState(indexMinute: event.indexMinute));
    }
  }

  Future addNotification(
      AddNotificationEvent event, Emitter<TaskDetailsState> emit) async {
    DateTime dateDueBy =
        DateTime(1970, 1, 1).add(Duration(seconds: event.taskModel.dueBy ?? 0));
    int secondInMinute = 60;
    Duration durationToReminder = dateDueBy.difference(DateTime.now());
    int durationInSeconds = durationToReminder.inSeconds -
        event.minuteBeforeList[event.indexMinute] * secondInMinute;
    int taskId = int.parse(event.taskModel.intId!);
    List<String>? notificationList = await _sharedPreference
        .getListStringsPreferenceValue(SharedPreferenceImpl.notificationList);
    if (!notificationList.contains(event.taskModel.intId.toString())) {
      notificationList.add(event.taskModel.intId.toString());
      NotificationService()
          .showNotification(taskId, event.taskModel.title!, durationInSeconds);
      _sharedPreference.setListStringsPreferenceValue(
          SharedPreferenceImpl.notificationList, notificationList);
      MessageUtils.toast(
          message: UiStrings.notificationAdded, type: MessageType.success);
    } else {
      MessageUtils.toast(
          message: UiStrings.notificationWasAdded, type: MessageType.info);
    }
  }
}
