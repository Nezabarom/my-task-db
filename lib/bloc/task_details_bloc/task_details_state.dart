import 'package:equatable/equatable.dart';

class TaskDetailsState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitTaskDetailsState extends TaskDetailsState {
  InitTaskDetailsState({required this.indexMinute});
  final int indexMinute;
  @override
  List<Object> get props => [indexMinute];
}

class DeleteTaskState extends TaskDetailsState{}