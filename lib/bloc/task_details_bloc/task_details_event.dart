// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class TaskDetailsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class DeleteTaskEvent extends TaskDetailsEvent{
  DeleteTaskEvent(this.taskModel);

  final TaskModel taskModel;
  @override
  List<Object> get props => [taskModel];
}

class IncrementMinuteEvent extends TaskDetailsEvent {
  IncrementMinuteEvent(this.indexMinute);
  int indexMinute;
  @override
  List<Object> get props => [indexMinute];
}

class DecrementMinuteEvent extends TaskDetailsEvent {
  DecrementMinuteEvent(this.indexMinute);
  int indexMinute;
  @override
  List<Object> get props => [indexMinute];
}

class AddNotificationEvent extends TaskDetailsEvent {
  AddNotificationEvent(this.taskModel, this.indexMinute, this.minuteBeforeList);
  TaskModel taskModel;
  int indexMinute;
  List<int> minuteBeforeList;
  @override
  List<Object> get props => [taskModel, indexMinute, minuteBeforeList];
}