import 'package:my_task_db/enums/sort_type_enum.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class SortingTaskList {

  static List<TaskModel> sort(List<TaskModel> taskList, SortType sortType){
    if (sortType == SortType.titleASC) {
      taskList.sort((a, b) => a.title!.compareTo(b.title!));
      taskList = taskList;
    }
    if (sortType == SortType.titleDESC) {
      taskList.sort((a, b) => b.title!.compareTo(a.title!));
      taskList = taskList;
    }
    if (sortType == SortType.dueByASC) {
      taskList.sort((a, b) => a.dueBy!.compareTo(b.dueBy!));
      taskList = taskList;
    }
    if (sortType == SortType.dueByDESC) {
      taskList.sort((a, b) => b.dueBy!.compareTo(a.dueBy!));
    }
    List<TaskModel> newTasksList = [];
    if (sortType == SortType.priorityASC) {
      for (var element in taskList) {
        if (element.priority == 'High') {
          newTasksList.add(element);
        }
      }
      for (var element in taskList) {
        if (element.priority == 'Normal') {
          newTasksList.add(element);
        }
      }
      for (var element in taskList) {
        if (element.priority == 'Low') {
          newTasksList.add(element);
          taskList = newTasksList;
        }
      }
    }
    if (sortType == SortType.priorityDESC) {
      List<TaskModel> newTasksList = [];
      for (var element in taskList) {
        if (element.priority == 'Low') {
          newTasksList.add(element);
        }
      }
      for (var element in taskList) {
        if (element.priority == 'Normal') {
          newTasksList.add(element);
        }
      }
      for (var element in taskList) {
        if (element.priority == 'High') {
          newTasksList.add(element);
          taskList = newTasksList;
        }
      }
    }
    return taskList;
  }

}