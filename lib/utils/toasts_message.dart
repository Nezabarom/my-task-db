import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/constants/ui_theme.dart';

enum MessageType { develop, success, error, info, warning }

class MessageUtils {
  static toast({String? message, MessageType? type}) {
    String text = "info";
    Color textColor = Colors.black87;
    Color backgroundColor = UiTheme.toastWarningBackgroundColor;
    Color iconColor = Colors.white;
    IconData iconData = Icons.info;

    if (message != null) {
      text = message;
    }

    if (type != null) {
      switch (type) {
        case MessageType.develop:
          backgroundColor = UiTheme.toastDevelopsBackgroundColor;
          iconData = Icons.developer_mode;
          iconColor = Colors.deepPurple;
          break;
        case MessageType.success:
          backgroundColor = UiTheme.toastSuccessBackgroundColor;
          iconData = Icons.check;
          iconColor = Colors.green;
          break;
        case MessageType.error:
          backgroundColor = UiTheme.toastErrorBackgroundColor;
          iconData = Icons.close;
          iconColor = Colors.red;
          break;
        case MessageType.info:
          backgroundColor = UiTheme.toastInfoBackgroundColor;
          iconData = Icons.info_outline;
          iconColor = Colors.blue;
          break;
        case MessageType.warning:
          backgroundColor = UiTheme.toastWarningBackgroundColor;
          iconData = Icons.warning_amber_outlined;
          iconColor = Colors.orange;
          break;
        default:
          break;
      }
    }

    BotToast.showAttachedWidget(
      attachedBuilder: (_) => Align(
        alignment: Alignment(0.w, -0.7.w),
        child: Padding(
          padding: EdgeInsets.all(32.0.w),
          child: Card(
            color: backgroundColor,
            child: Padding(
              padding: EdgeInsets.all(8.0.w),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    iconData,
                    color: iconColor,
                  ),
                  const SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Text(
                      text,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: textColor,),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      duration: const Duration(seconds: 3),
      target: const Offset(0, 0),
    );
  }
}
