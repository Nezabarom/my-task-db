import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/bloc/auth_bloc/login_page_bloc.dart';
import 'package:my_task_db/bloc/home_bloc/home_bloc.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_bloc.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_event.dart';
import 'package:my_task_db/bloc/settings_bloc/settings_bloc.dart';
import 'package:my_task_db/bloc/settings_bloc/settings_event.dart';
import 'package:my_task_db/bloc/task_details_bloc/task_details_bloc.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_bloc.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_event.dart';
import 'package:my_task_db/constants/ui_theme.dart';
import 'package:my_task_db/data/repository/network/auth/auth_repository_impl.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';
import 'package:my_task_db/ui/login_page/login_page.dart';
import 'package:my_task_db/ui/main_page/home_page.dart';
import 'package:my_task_db/utils/local_notification.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:flutter_locales/flutter_locales.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  NotificationService().initNotification();
  await Locales.init(['en', 'uk']);
  tz.initializeTimeZones();
  SharedPreference pref = SharedPreferenceImpl();
  bool isLogin = await pref.getBoolPreferenceValue(SharedPreferenceImpl.isUserExist);
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      systemNavigationBarColor: UiTheme.statusBarColor,
      systemNavigationBarDividerColor: UiTheme.firstTypeBackgroundColor,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor: UiTheme.statusBarColor,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.light,
    ),
  );
  runApp(MyApp(isLogin: isLogin));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.isLogin}) : super(key: key);

  final bool isLogin;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      builder: (context, _){
        return MultiRepositoryProvider(
          providers: [
            RepositoryProvider(create: (context) => AuthRepository()),
            RepositoryProvider(create: (context) => TaskRepository())
          ],
          child: MultiBlocProvider(
            providers: [
              BlocProvider<AuthBloc>(
                create: (BuildContext context) => AuthBloc(authRepository: RepositoryProvider.of<AuthRepository>(context)),
              ),
              BlocProvider<TaskListBloc>(
                create: (BuildContext context) => TaskListBloc(taskRepository: RepositoryProvider.of<TaskRepository>(context))..add(GetTaskListEvent()),
              ),
              BlocProvider<NotificationListBloc>(
                create: (BuildContext context) => NotificationListBloc(taskRepository: RepositoryProvider.of<TaskRepository>(context))..add(GetNotificationListEvent()),
              ),
              BlocProvider<SettingsBloc>(
                create: (BuildContext context) => SettingsBloc()..add(GetDefaultSortValueEvent(context)),
              ),
              BlocProvider<TaskDetailsBloc>(
                create: (BuildContext context) => TaskDetailsBloc(taskRepository: RepositoryProvider.of<TaskRepository>(context)),
              ),
              BlocProvider<HomeBloc>(
                create: (BuildContext context) => HomeBloc(),
              ),
            ],
            child: LocaleBuilder(
              builder: (locale) {
                return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  builder: BotToastInit(),
                  navigatorObservers: [BotToastNavigatorObserver()],
                  localizationsDelegates: Locales.delegates,
                  supportedLocales: Locales.supportedLocales,
                  locale: locale,
                  home: (isLogin) ? const HomePage() : const LoginPage(),
                );
              },
            ),
          ),
        );
      },
    );
  }
}