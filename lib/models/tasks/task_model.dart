class TaskModel {
  TaskModel({
    required  this.intId,
    required  this.stringId,
    required  this.title,
    required  this.dueBy,
    required  this.priority,
  });

  String? intId;
  String? stringId;
  String? title;
  int? dueBy;
  String? priority;

  factory TaskModel.fromJson(Map<String, dynamic> json) => TaskModel(
    intId: json["intId"],
    stringId: json['stringId'],
    title: json["title"],
    dueBy: json["dueBy"],
    priority: json["priority"],
  );

  factory TaskModel.fromMap(Map<dynamic, dynamic> map) {
    return TaskModel(
      intId: map["intId"] ?? '',
      stringId: map['stringId'] ?? '',
      title: map['title'] ?? '',
      dueBy: map['dueBy'] ?? '',
      priority: map['priority'] ?? '',
    );
  }

  Map<String, dynamic> toJson() => {
    "intId": intId,
    'stringId': stringId,
    "title": title,
    "dueBy": dueBy,
    "priority": priority,
  };
}