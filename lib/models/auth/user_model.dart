class UserModel {
  String? id;
  String? email;
  String? password;

  UserModel({this.id, this.email, this.password});

  UserModel.fromJson(var value){
    id = value['id'] is String?value["id"]:value["id"].toString();
    email = value['email'] is String?value["email"]:value["email"].toString();
    password = value['password'] is String?value["password"]:value["password"].toString();

  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "email": email,
    "password": password,
  };
}
