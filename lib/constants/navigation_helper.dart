import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_bloc.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_event.dart';
import 'package:my_task_db/models/tasks/task_model.dart';
import 'package:my_task_db/ui/create_task_page/create_task_page.dart';
import 'package:my_task_db/ui/edit_task_page/edit_task_page.dart';
import 'package:my_task_db/ui/login_page/login_page.dart';
import 'package:my_task_db/ui/main_page/home_page.dart';
import 'package:my_task_db/ui/task_details_page/task_details_page.dart';

class NavigationHelper {

  static void openMainPage (BuildContext context) async {
    context.read<TaskListBloc>().add(GetTaskListEvent());
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const HomePage()),
          (Route<dynamic> route) => false,
    );
  }

  static void openLoginPage (BuildContext context) {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const LoginPage()),
          (Route<dynamic> route) => false,
    );
  }

  static void openTaskDetailsPage (BuildContext context, TaskModel model) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => TaskDetailsPage(model)));
  }

  static void openCreateTaskPage (BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => const CreateTaskPage()));
  }

  static void openEditTaskPage (BuildContext context, TaskModel model) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => EditTaskPage(model)));
  }

}