import 'dart:ui';

class UiTheme {

  static const Color firstTypeBackgroundColor = Color(0xFF607D8B);
  static const Color secondTypeBackgroundColor = Color(0xFF90A4AE);
  static const Color darkRedColor = Color(0xFF901515);
  static const Color statusBarColor = Color(0x44605D5D);

  //toast backgrounds colors
  static const Color toastInfoBackgroundColor = Color(0xFFBAC4FF);
  static const Color toastSuccessBackgroundColor = Color(0xFFBBFFBA);
  static const Color toastDevelopsBackgroundColor = Color(0xFFFFBAFC);
  static const Color toastWarningBackgroundColor = Color(0xFFFFEFBA);
  static const Color toastErrorBackgroundColor = Color(0xFFFFE3E2);

}