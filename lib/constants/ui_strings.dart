class UiStrings {

  static String registrationButtonText = 'registration';
  static String noData = 'no_data';
  static String loginButtonText = 'log_in';
  static String english = 'english';
  static String ukrainian = 'ukrainian';
  static String taskDetails = 'task_details';
  static String editTask = 'edit_task';
  static String deleteTask = 'delete_task';
  static String dueBy = 'due_by';
  static String priority = 'priority';
  static String notification = 'notification';
  static String minuteBefore = 'minute_before';
  static String notificationWasAdded = 'notification_was_added';
  static String add = 'add';
  static String sortBy = 'sort_by';
  static String nameDialog = 'name_dialog';
  static String priorityDialog = 'priority_dialog';
  static String dateDialog = 'date_dialog';
  static String doNotHaveAccount = 'do_not_have_account';
  static String toRegister = 'to_register';
  static String toLogIn = 'to_log_in';
  static String createTask = 'create_task';
  static String titleDescription = 'title_description';
  static String date = 'date';
  static String time = 'time';
  static String notPicked = 'not_picked';
  static String pickDate = 'pick_date';
  static String pickTime = 'pick_time';
  static String pickPriority = 'pick_priority';
  static String pickTitleDescription = 'pick_title_description';
  static String pickDateAndTime = 'Pick date and time';
  static String addTask = 'Add task';
  static String low = 'low';
  static String normal = 'normal';
  static String high = 'high';
  static String saveTask = 'save_task';
  static String removeAll = 'remove_all';
  static String remove = 'Remove';
  static String youDontHaveAnyNotificationsYet = "you_don't_have_any_notifications_yet";
  static String listOfTasks = "list_of_tasks";
  static String settings = "settings";
  static String pickDefaultSorting = "pick_default_sorting";
  static String pickLanguage = "pick_language";
  static String logOut = "log_out";
  static String notificationAdded = "notification_added";
  static String email = "email";
  static String password = "password";

  //FireBaseErrorCode
  static String firebaseAuthUtilEmailAlreadyInUse = "email-already-in-use";
  static String firebaseAuthUtilsWeakPassword = "weak-password";
  static String firebaseAuthUtilWrongPassword = "wrong-password";
  static String firebaseAuthUtilUserNotFound = "user-not-found";

  //FireBaseAuthUtils
  static String firebaseAuthUtilsSomethingIsWrong = "something_is_wrong";
  static String firebaseAuthUtilPasswordProvidedTooWeak = "password_provided_too_weak";
  static String firebaseAuthUtilNoUserFoundForThatEmail = "no_user_found_for_that_email";
  static String firebaseAuthUtilWrongPasswordProvidedForThatUser = "wrong_password_provided_for_that_user";
  static String firebaseAuthUtilTheAccount = "account_already_exists_for_that_email";

}