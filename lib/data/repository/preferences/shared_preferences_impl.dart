import 'package:my_task_db/enums/sort_type_enum.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'shared_preferences.dart';

class SharedPreferenceImpl implements SharedPreference{

  static const String token = "token";
  static const String isUserExist = "isUserExist";
  static const String defaultSortType = "defaultSortType";
  static const String notificationList = "notificationList";
  static const String tasksIdList = "tasksIdList";

  @override
  setStringPreferenceValue(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  @override
  setIntPreferenceValue(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, value);
  }

  @override
  setBoolPreferenceValue(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
  }

  @override
  setListStringsPreferenceValue(String key, List<String> value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList(key, value);
  }

  @override
  Future<String> getStringPreferenceValue(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? "";
  }

  @override
  Future<SortType> getDefaultSortType(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value = prefs.getString(key) ?? "";
    late SortType sortType;
    if (value == 'priority_dialog') {
      sortType = SortType.priorityASC;
    }
    if (value == 'date_dialog') {
      sortType = SortType.dueByASC;
    }
    if (value == 'name_dialog') {
      sortType = SortType.titleASC;
    }
     if (value == '') {
      sortType = SortType.dueByASC;
    }
    return sortType;
  }

  @override
  Future<int> getIntPreferenceValue(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key) ?? 0;
  }

  @override
  Future<bool> getBoolPreferenceValue(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key) ?? false;
  }

  @override
  Future<List<String>> getListStringsPreferenceValue(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(key) ?? [];
  }

  @override
  Future removePreferenceValue(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(key);
  }

}
