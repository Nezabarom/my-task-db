import 'package:my_task_db/enums/sort_type_enum.dart';

abstract class SharedPreference {

  setStringPreferenceValue(String key, String value);

  setIntPreferenceValue(String key, int value);

  setBoolPreferenceValue(String key, bool value);

  setListStringsPreferenceValue(String key, List<String> value);

  Future<String> getStringPreferenceValue(String key);

  Future<SortType> getDefaultSortType(String key);

  Future<int> getIntPreferenceValue(String key);

  Future<bool> getBoolPreferenceValue(String key);

  Future<List<String>> getListStringsPreferenceValue(String key);

  Future removePreferenceValue(String key);

}