import 'package:firebase_database/firebase_database.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class TaskRepository {

  final myDatabase = FirebaseDatabase.instance.ref().child("user_table");

  Future createTask(String userId, TaskModel model) async {
    await myDatabase.child(userId).child("tasks_table").push().set(model.toJson());
  }

  Future <List<TaskModel>?> getTasksList(String userId) async {
    List<TaskModel> tasks = [];
    DataSnapshot? snapshot = await myDatabase.child("$userId/tasks_table").get();
    if (snapshot.exists) {
      final map = snapshot.value as Map<dynamic, dynamic>;
      map.forEach((key, value) {
        final model = TaskModel.fromMap(value);
        model.stringId = key;
        tasks.add(model);
      });
      return tasks;
    } else {
      return null;
    }
  }

  Future <TaskModel> getTaskView(String userId, String taskId) async {
    DatabaseEvent snapshot = await myDatabase.child('$userId/tasks_table/$taskId').once();
    late TaskModel model;
    final map = snapshot.snapshot as Map<dynamic, dynamic>;
    map.forEach((key, value) {
      model = TaskModel.fromJson(value);
    });
    return model;
  }

  Future editTask(String userId, TaskModel model) async {
    await myDatabase.child('$userId/tasks_table/${model.stringId}').update(model.toJson());
  }

  Future deleteTask(String userId, String taskId) async {
    await myDatabase.child('$userId/tasks_table/$taskId').remove();
  }

}