import 'package:my_task_db/constants/ui_strings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences.dart';
import 'package:my_task_db/data/repository/preferences/shared_preferences_impl.dart';
import 'package:my_task_db/models/auth/user_model.dart';
import 'package:my_task_db/utils/toasts_message.dart';
import 'package:firebase_database/firebase_database.dart';

class AuthRepository {
  final myDatabase = FirebaseDatabase.instance.ref().child("user_table");
  final SharedPreference _sharedPreference = SharedPreferenceImpl();

  Future<bool> logIn(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);

      User? user = userCredential.user;
      if (user != null) {
        _sharedPreference.setStringPreferenceValue(
            SharedPreferenceImpl.token, user.uid);
        _sharedPreference.setBoolPreferenceValue(
            SharedPreferenceImpl.isUserExist, true);
        return true;
      }
    } on FirebaseAuthException catch (error) {
      print(error);
      if (error.code == UiStrings.firebaseAuthUtilUserNotFound) {
        MessageUtils.toast(
            message: UiStrings.firebaseAuthUtilNoUserFoundForThatEmail);
      } else if (error.code == UiStrings.firebaseAuthUtilWrongPassword) {
        MessageUtils.toast(
            message:
                UiStrings.firebaseAuthUtilWrongPasswordProvidedForThatUser);
      } else {
        MessageUtils.toast(
            message: UiStrings.firebaseAuthUtilsSomethingIsWrong);
      }
    }
    return false;
  }

  Future<bool> registration(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);

      User? user = userCredential.user;
      if (user != null) {
        _sharedPreference.setStringPreferenceValue(
            SharedPreferenceImpl.token, user.uid);
        _sharedPreference.setBoolPreferenceValue(
            SharedPreferenceImpl.isUserExist, true);
        return true;
      }
    } on FirebaseAuthException catch (error) {
      print(error);
      if (error.code == UiStrings.firebaseAuthUtilsWeakPassword) {
        MessageUtils.toast(
            message: UiStrings.firebaseAuthUtilPasswordProvidedTooWeak);
      } else if (error.code == UiStrings.firebaseAuthUtilEmailAlreadyInUse) {
        MessageUtils.toast(message: UiStrings.firebaseAuthUtilTheAccount);
      } else {
        MessageUtils.toast(
            message: UiStrings.firebaseAuthUtilsSomethingIsWrong);
      }
    }
    return false;
  }

  Future addUser(UserModel model) async {
    String userId = model.id!;
    await myDatabase.child(userId).set(model.toJson());
  }

  Future<UserModel?> getUser(String id) async {
    DatabaseEvent snapshot = await myDatabase.child(id).once();
    if (snapshot.snapshot.exists) {
      UserModel model = UserModel.fromJson(snapshot.snapshot);
      return model;
    } else {
      return null;
    }
  }
}
