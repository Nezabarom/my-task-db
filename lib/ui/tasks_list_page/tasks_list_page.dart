import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_bloc.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_event.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_state.dart';
import 'package:my_task_db/constants/navigation_helper.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/constants/ui_theme.dart';
import 'package:my_task_db/enums/sort_type_enum.dart';
import 'package:my_task_db/models/tasks/task_model.dart';
import 'package:my_task_db/ui/dialogs/sorting_dialog.dart';
import 'package:my_task_db/ui/widgets/loading_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TasksListPage extends StatelessWidget {
  TasksListPage({Key? key}) : super(key: key);

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.secondTypeBackgroundColor,
      appBar: AppBar(
        toolbarHeight: 50.0.w,
        leadingWidth: 50.w,
        automaticallyImplyLeading: false,
        backgroundColor: UiTheme.firstTypeBackgroundColor,
        centerTitle: true,
        title: LocaleText(UiStrings.listOfTasks,
            style: TextStyle(fontSize: 16.sp)),
        leading: InkWell(
            onTap: () {
              NavigationHelper.openCreateTaskPage(context);
            },
            child: Padding(
              padding: EdgeInsets.only(left: 15.w),
              child: Icon(
                Icons.create_new_folder,
                size: 20.w,
              ),
            )),
        actions: [
          BlocBuilder<TaskListBloc, TaskListState>(
            builder: (context, state) {
              if (state is GetTaskListState) {
                return InkWell(
                  onTap: () {
                    SortType? changedSortType;
                    SortingDialog.openSortingDialog(
                        context, changedSortType ?? state.sortType,
                        (sortTypeCalBack) {
                      changedSortType = sortTypeCalBack;
                      context.read<TaskListBloc>().add(SortedListEvent(
                          changedSortType ?? state.sortType, state.taskList));
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.only(right: 15.w),
                    child: Icon(
                      Icons.sort,
                      size: 20.w,
                    ),
                  ),
                );
              }
              return const SizedBox();
            },
          )
        ],
      ),
      body: _bodyWidget(),
    );
  }

  Widget _bodyWidget() {
    return BlocBuilder<TaskListBloc, TaskListState>(builder: (context, state) {
      if (state is GetTaskListState) {
        if (state.taskList.isEmpty) {
          return Center(
            child: GestureDetector(
              onTap: () {
                NavigationHelper.openCreateTaskPage(context);
              },
              child: Container(
                height: 30.w,
                width: 100.w,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.w),
                  color: Colors.blueGrey,
                ),
                child: Center(
                  child: Text(UiStrings.addTask,
                      style: TextStyle(fontSize: 14.sp)),
                ),
              ),
            ),
          );
        } else {
          return ListView.builder(
              controller: _scrollController,
              physics: const BouncingScrollPhysics(),
              itemCount: state.taskList.length,
              itemBuilder: (context, index) {
                return _taskItem(state.taskList[index], context);
              });
        }
      }
      if (state is InitTaskListState) {
        return const LoadingWidget();
      }
      return const SizedBox();
    });
  }

  Widget _taskItem(TaskModel model, BuildContext context) {
    DateTime dateDueBy =
        DateTime(1970, 1, 1).add(Duration(seconds: model.dueBy ?? 0));
    return GestureDetector(
      onTap: () {
        NavigationHelper.openTaskDetailsPage(context, model);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(10.w, 10.w, 10.w, 0.w),
        padding: EdgeInsets.symmetric(vertical: 15.w, horizontal: 25.w),
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.w),
            color: UiTheme.firstTypeBackgroundColor),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(model.title ?? UiStrings.noData,
                style: TextStyle(fontSize: 14.sp)),
            SizedBox(
              height: 10.w,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    LocaleText(UiStrings.dueBy,
                        style: TextStyle(fontSize: 14.sp)),
                    SizedBox(
                      width: 20.w,
                    ),
                    Text(
                        '${dateDueBy.month}/${dateDueBy.day}/${dateDueBy.year}',
                        style: TextStyle(fontSize: 14.sp)),
                  ],
                ),
                SizedBox(
                  width: 20.w,
                ),
                Text(model.priority ?? UiStrings.noData,
                    style: TextStyle(fontSize: 14.sp))
              ],
            )
          ],
        ),
      ),
    );
  }
}
