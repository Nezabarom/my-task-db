import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/bloc/edit_task_bloc/edit_task_bloc.dart';
import 'package:my_task_db/bloc/edit_task_bloc/edit_task_event.dart';
import 'package:my_task_db/bloc/edit_task_bloc/edit_task_state.dart';
import 'package:my_task_db/constants/navigation_helper.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/constants/ui_theme.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class EditTaskPage extends StatefulWidget {
  const EditTaskPage(this.taskModel, {Key? key}) : super(key: key);

  final TaskModel taskModel;

  @override
  _EditTaskPageState createState() => _EditTaskPageState();
}

class _EditTaskPageState extends State<EditTaskPage> {
  late final TextEditingController _titleController =
      TextEditingController(text: widget.taskModel.title);
  late DateTime _taskDate =
      DateTime(1970, 1, 1).add(Duration(seconds: widget.taskModel.dueBy!));
  late TimeOfDay _taskTime =
      TimeOfDay(hour: _taskDate.hour, minute: _taskDate.minute);
  final int secondInDay = 86400;
  late int days = widget.taskModel.dueBy! ~/ secondInDay;
  late final int? _timeTDate = days * secondInDay;
  late final int? _timeTTime = widget.taskModel.dueBy! - _timeTDate!;
  late TaskModel newTaskModel;

  @override
  void dispose() {
    _titleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EditTaskBloc>(
      create: (BuildContext context) => EditTaskBloc(taskRepository: RepositoryProvider.of<TaskRepository>(context))
        ..add(EditTaskInitEvent(
            timeTimeT: _timeTTime!,
            dateTimeT: _timeTDate!,
            priority: widget.taskModel.priority!)),
      child: Scaffold(
        backgroundColor: UiTheme.secondTypeBackgroundColor,
        appBar: AppBar(
          toolbarHeight: 50.w,
          leadingWidth: 50.w,
          centerTitle: true,
          backgroundColor: Colors.blueGrey,
          leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: Icon(
                  Icons.arrow_back,
                  size: 20.w,
                ),
              )),
          title:
              LocaleText(UiStrings.editTask, style: TextStyle(fontSize: 16.sp)),
        ),
        body: Container(
          padding: EdgeInsets.all(25.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              LocaleText(UiStrings.titleDescription,
                  style: TextStyle(fontSize: 14.sp)),
              SizedBox(
                height: 15.w,
              ),
              _titleTextField(),
              SizedBox(
                height: 15.w,
              ),
              LocaleText(UiStrings.pickDate, style: TextStyle(fontSize: 14.sp)),
              SizedBox(
                height: 15.w,
              ),
              _datePicker(context),
              SizedBox(
                height: 15.w,
              ),
              LocaleText(UiStrings.time, style: TextStyle(fontSize: 14.sp)),
              SizedBox(
                height: 15.w,
              ),
              _timePicker(context),
              SizedBox(
                height: 15.w,
              ),
              LocaleText(UiStrings.priority, style: TextStyle(fontSize: 14.sp)),
              SizedBox(
                height: 15.w,
              ),
              BlocBuilder<EditTaskBloc, EditTaskState>(
                  buildWhen: (previous, state) {
                return state is EditTaskPrioritySelectorState;
              }, builder: (context, state) {
                if (state is EditTaskPrioritySelectorState) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _priorityButton(0, state.prioritySelectorList[0],
                          UiStrings.low, state.prioritySelectorList, context),
                      _priorityButton(
                          1,
                          state.prioritySelectorList[1],
                          UiStrings.normal,
                          state.prioritySelectorList,
                          context),
                      _priorityButton(2, state.prioritySelectorList[2],
                          UiStrings.high, state.prioritySelectorList, context)
                    ],
                  );
                }
                return const SizedBox();
              })
            ],
          ),
        ),
        bottomNavigationBar: _saveTaskButton(context),
      ),
    );
  }

  Widget _titleTextField() {
    return TextField(
      controller: _titleController,
      enableInteractiveSelection: true,
      textInputAction: TextInputAction.go,
      maxLines: 3,
      autofocus: false,
      style: TextStyle(fontSize: 14.sp),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
        filled: true,
        fillColor: Colors.blueGrey,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(10.w),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(10.w),
        ),
      ),
    );
  }

  Widget _datePicker(BuildContext context) {
    return BlocBuilder<EditTaskBloc, EditTaskState>(
        buildWhen: (previous, state) {
      return state is EditTaskPickDateState;
    }, builder: (context, state) {
      if (state is EditTaskPickDateState) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              child: Center(
                  child: Text(
                      '${_taskDate.month}/${_taskDate.day}/${_taskDate.year}',
                    style: TextStyle(fontSize: 14.sp),)),
              height: 50.w,
              width: 120.w,
              decoration: BoxDecoration(
                  color: UiTheme.firstTypeBackgroundColor,
                  borderRadius: BorderRadius.circular(10.w)),
            ),
            InkWell(
              onTap: () {
                showDatePicker(
                        context: context,
                        initialDate: _taskDate,
                        firstDate: DateTime.now().add(const Duration(days: 1)),
                        lastDate: DateTime(2038, 1, 19))
                    .then((data) {
                  if (data != null) {
                    _taskDate = data;
                    context
                        .read<EditTaskBloc>()
                        .add(EditTaskPickDateEvent(data));
                  }
                });
              },
              child: Container(
                height: 40.w,
                width: 120.w,
                decoration: BoxDecoration(
                    color: UiTheme.firstTypeBackgroundColor,
                    borderRadius: BorderRadius.circular(10.w)),
                child: Center(
                  child: LocaleText(UiStrings.pickDate,
                      style: TextStyle(fontSize: 14.sp)),
                ),
              ),
            )
          ],
        );
      }
      return const SizedBox();
    });
  }

  Widget _timePicker(BuildContext context) {
    return BlocBuilder<EditTaskBloc, EditTaskState>(
        buildWhen: (previous, state) {
      return state is EditTaskPickTimeState;
    }, builder: (context, state) {
      if (state is EditTaskPickTimeState) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              child: Center(
                  child: Text((_taskTime.minute > 9)
                      ? '${_taskTime.hour} : ${_taskTime.minute}'
                      : '${_taskTime.hour} : 0${_taskTime.minute}',
                    style: TextStyle(fontSize: 14.sp),)),
              height: 50.w,
              width: 120.w,
              decoration: BoxDecoration(
                  color: UiTheme.firstTypeBackgroundColor,
                  borderRadius: BorderRadius.circular(10.w)),
            ),
            InkWell(
              onTap: () {
                showTimePicker(
                        context: context,
                        initialTime: const TimeOfDay(hour: 09, minute: 00))
                    .then((time) {
                  if (time != null) {
                    _taskTime = time;
                    context
                        .read<EditTaskBloc>()
                        .add(EditTaskPickTimeEvent(time));
                  }
                });
              },
              child: Container(
                height: 40.w,
                width: 120.w,
                decoration: BoxDecoration(
                    color: UiTheme.firstTypeBackgroundColor,
                    borderRadius: BorderRadius.circular(10.w)),
                child: Center(
                  child: LocaleText(UiStrings.pickTime,
                      style: TextStyle(fontSize: 14.sp)),
                ),
              ),
            )
          ],
        );
      }
      return const SizedBox();
    });
  }

  Widget _priorityButton(int index, bool isSelected, String textButton,
      List<bool> priorityList, BuildContext context) {
    return InkWell(
      onTap: () {
        context
            .read<EditTaskBloc>()
            .add(EditTaskPrioritySelectorEvent(index, priorityList));
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 3.w, color: Colors.blueGrey),
            color: (isSelected) ? Colors.blueGrey : Colors.transparent),
        width: 100.w,
        height: 25.w,
        child: Center(
            child: LocaleText(textButton, style: TextStyle(fontSize: 14.sp))),
      ),
    );
  }

  String _priorityList(List<bool> prioritySelectorList) {
    late String priority;
    if (prioritySelectorList[0] == true) {
      priority = 'Low';
    }
    if (prioritySelectorList[1] == true) {
      priority = 'Normal';
    }
    if (prioritySelectorList[2] == true) {
      priority = 'High';
    }
    return priority;
  }

  Widget _saveTaskButton(BuildContext context) {
    return BlocConsumer<EditTaskBloc, EditTaskState>(
        buildWhen: (previous, state) {
      return state is EditTaskPrioritySelectorState;
    }, builder: (context, state) {
      if (state is EditTaskPrioritySelectorState) {
        return InkWell(
          onTap: () {
            String priority = _priorityList(state.prioritySelectorList);
            String title = _titleController.text;
            int timeT = changeDateAndTimeToTimeT(_taskDate, _taskTime);
            context.read<EditTaskBloc>().add(EditTaskTapEvent(
                TaskModel(
                    intId: widget.taskModel.intId,
                    stringId: widget.taskModel.stringId,
                    title: title,
                    dueBy: timeT,
                    priority: priority),
                state.prioritySelectorList));
            newTaskModel = TaskModel(
                intId: widget.taskModel.intId,
                stringId: widget.taskModel.stringId,
                title: title,
                dueBy: timeT,
                priority: priority);
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(10.w, 0, 10, 10.w),
            width: double.infinity,
            height: 50.w,
            decoration: BoxDecoration(
              color: Colors.blueGrey,
              borderRadius: BorderRadius.circular(10.w),
            ),
            child: Center(
                child: LocaleText(
              UiStrings.saveTask,
              style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w700),
            )),
          ),
        );
      }
      return const SizedBox();
    }, listener: (context, state) {
      if (state is EditTaskTapState) {
        NavigationHelper.openTaskDetailsPage(context, newTaskModel);
      }
    });
  }

  int changeDateAndTimeToTimeT(DateTime pickedDate, TimeOfDay pickedTime){
      DateTime startEpochDAte = DateTime(1970, 1, 1);
      Duration? differenceDate = pickedDate.difference(startEpochDAte);
      Duration differenceTime = Duration(hours: pickedTime.hour, minutes: pickedTime.minute);
      int timeT = differenceDate.inSeconds + differenceTime.inSeconds;
      return timeT;
  }
}
