import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/bloc/settings_bloc/settings_bloc.dart';
import 'package:my_task_db/bloc/settings_bloc/settings_event.dart';
import 'package:my_task_db/bloc/settings_bloc/settings_state.dart';
import 'package:my_task_db/constants/navigation_helper.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/constants/ui_theme.dart';

class SettingsPage extends StatelessWidget {
  SettingsPage({Key? key}) : super(key: key);

  final List<String> _sortList = [
    UiStrings.nameDialog,
    UiStrings.dateDialog,
    UiStrings.priorityDialog
  ];
  final List<String> _languageList = [UiStrings.english, UiStrings.ukrainian];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.secondTypeBackgroundColor,
      appBar: AppBar(
        toolbarHeight: 50.w,
        backgroundColor: UiTheme.firstTypeBackgroundColor,
        centerTitle: true,
        title:
        LocaleText(UiStrings.settings, style: TextStyle(fontSize: 16.sp)),
      ),
      body: Container(
        padding: EdgeInsets.all(20.w),
        child: BlocConsumer<SettingsBloc, SettingState>(
          listener: (context, state) {
            if (state is LogOutState) {
              NavigationHelper.openLoginPage(context);
            }
          },
          builder: (context, state) {
            if (state is GetDefaultSortValueState) {
              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          flex: 5,
                          child: LocaleText(UiStrings.pickDefaultSorting,
                              style: TextStyle(fontSize: 14.sp))),
                      Expanded(
                          flex: 3,
                          child: _defaultSortTypePicker(state, context))
                    ],
                  ),
                  SizedBox(height: 10.w,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          flex: 5,
                          child: LocaleText(UiStrings.pickLanguage,
                              style: TextStyle(fontSize: 14.sp))),
                      Expanded(flex: 3, child: _languagePicker(context))
                    ],
                  ),
                ],
              );
            }
            return const SizedBox();
          },
        ),
      ),
      bottomNavigationBar: _logOutButton(),
    );
  }

  Widget _defaultSortTypePicker(
      GetDefaultSortValueState state, BuildContext context) {
    return DropdownButton(
      isExpanded: true,
      underline: Container(
        height: 2.w,
        color: UiTheme.firstTypeBackgroundColor,
      ),
      iconEnabledColor: UiTheme.firstTypeBackgroundColor,
      dropdownColor: UiTheme.firstTypeBackgroundColor,
      hint: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        child: LocaleText(state.defaultSortValue,
            style: TextStyle(fontSize: 14.sp)),
      ),
      items: _sortList.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: LocaleText(value, style: TextStyle(fontSize: 14.sp)),
        );
      }).toList(),
      onChanged: (String? newValue) {
        context.read<SettingsBloc>().add(SortingDropDownValueEvent(newValue!));
      },
    );
  }

  Widget _languagePicker(BuildContext context) {
    String? languageValue = Locales.currentLocale(context)!.languageCode;
    return DropdownButton(
        isExpanded: true,
        underline: Container(
          height: 2.w,
          color: UiTheme.firstTypeBackgroundColor,
        ),
        iconEnabledColor: UiTheme.firstTypeBackgroundColor,
        dropdownColor: UiTheme.firstTypeBackgroundColor,
        hint: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: Text(languageValue, style: TextStyle(fontSize: 14.sp)),
        ),
        items: _languageList.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: LocaleText(value, style: TextStyle(fontSize: 14.sp)),
          );
        }).toList(),
        onChanged: (String? newValue) {
          languageValue = newValue ?? languageValue;
          setNewLanguage(languageValue!, context);
        });
  }

  Widget _logOutButton() {
    return BlocBuilder<SettingsBloc, SettingState>(builder: (context, state) {
      return InkWell(
        onTap: () {
          context.read<SettingsBloc>().add(LogOutEvent());
        },
        child: Container(
          margin: EdgeInsets.fromLTRB(10.w, 0, 10, 10.w),
          width: double.infinity,
          height: 50.w,
          decoration: BoxDecoration(
            color: UiTheme.darkRedColor,
            borderRadius: BorderRadius.circular(10.w),
          ),
          child: Center(
              child: LocaleText(
                UiStrings.logOut,
                style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w700),
              )),
        ),
      );
    });
  }

  setNewLanguage(String languageDropdownValue, BuildContext context) {
    if (languageDropdownValue == UiStrings.english) {
      Locales.change(context, 'en');
    }
    if (languageDropdownValue == UiStrings.ukrainian) {
      Locales.change(context, 'uk');
    }
  }
}
