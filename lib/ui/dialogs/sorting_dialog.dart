import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/enums/sort_type_enum.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SortingDialog {
  static void openSortingDialog(
      BuildContext context, SortType sortType, Function function) {
    showDialog(
      context: context,
      builder: (context) {
        return AddSortingDialog(sortType, (sortType) {
          function(sortType);
        });
      },
    );
  }
}

// ignore: must_be_immutable
class AddSortingDialog extends StatelessWidget {
  AddSortingDialog(this.sortType, this.function, {Key? key}) : super(key: key);

  SortType sortType;
  final Function function;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      insetPadding:
      EdgeInsets.only(left: 235.w, right: 15.w, top: 15.w, bottom: 350.w),
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(15.w),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.w),
                  color: Theme.of(context).scaffoldBackgroundColor),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  LocaleText(
                    UiStrings.sortBy,
                    style: TextStyle(
                        fontSize: 17.sp,
                        color: Colors.black.withOpacity(0.5),
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 15.w,
                  ),
                  Divider(
                    color: Theme.of(context).hintColor,
                    height: 1,
                    thickness: 1,
                  ),
                  SizedBox(
                    height: 15.w,
                  ),
                  _nameSortButton(context),
                  SizedBox(
                    height: 15.w,
                  ),
                  _prioritySortButton(context),
                  SizedBox(
                    height: 15.w,
                  ),
                  _dateSortButton(context)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _nameSortButton(BuildContext context) {
    return InkWell(
      onTap: () {
        if (sortType == SortType.titleASC) {
          sortType = SortType.titleDESC;
        } else {
          sortType = SortType.titleASC;
        }
        function(sortType);
        Navigator.pop(context);
      },
      child: LocaleText(
        UiStrings.nameDialog,
        style: TextStyle(fontSize: 17.sp, fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget _prioritySortButton(BuildContext context) {
    return InkWell(
      onTap: () {
        if (sortType == SortType.priorityDESC) {
          sortType = SortType.priorityASC;
        } else {
          sortType = SortType.priorityDESC;
        }
        function(sortType);
        Navigator.pop(context);
      },
      child: LocaleText(
        UiStrings.priorityDialog,
        style: TextStyle(fontSize: 17.sp, fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget _dateSortButton(BuildContext context) {
    return InkWell(
      onTap: () {
        if (sortType == SortType.dueByASC) {
          sortType = SortType.dueByDESC;
        } else {
          sortType = SortType.dueByASC;
        }
        function(sortType);
        Navigator.pop(context);
      },
      child: LocaleText(
        UiStrings.dateDialog,
        style: TextStyle(fontSize: 17.sp, fontWeight: FontWeight.w700),
      ),
    );
  }
}
