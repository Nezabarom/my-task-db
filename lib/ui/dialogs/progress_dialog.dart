import 'package:flutter/material.dart';
import 'package:my_task_db/ui/widgets/loading_widget.dart';

class ProgressDialog {
  static Future showWithKey(BuildContext context, GlobalKey key) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () async => true,
              child: Theme(
                data: Theme.of(context).copyWith(
                  canvasColor: Colors.transparent,
                  shadowColor: Colors.transparent,
                ),
                child: SimpleDialog(key: key, backgroundColor: Colors.transparent, children: const <Widget>[
                  Center(
                    child: LoadingWidget(),
                  )
                ]),
              ));
        });
  }

  static void showProgress(BuildContext context, var key, bool isProgressShow) {
    if (isProgressShow) {
      ProgressDialog.showWithKey(context, key);
    } else {
      Navigator.of(key.currentContext, rootNavigator: true).pop();
    }
  }
}
