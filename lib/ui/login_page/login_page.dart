import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/bloc/auth_bloc/login_page_bloc.dart';
import 'package:my_task_db/bloc/auth_bloc/login_page_event.dart';
import 'package:my_task_db/bloc/auth_bloc/login_page_state.dart';
import 'package:my_task_db/constants/navigation_helper.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/constants/ui_theme.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.secondTypeBackgroundColor,
      body: BlocListener<AuthBloc, AuthState>(
        child: _bodyWidget(),
        listener: (context, state) {
          if (state is LoginState) {
            if (state.isLogin) {
              NavigationHelper.openMainPage(context);
            }
          }
          if (state is RegistrationState) {
            if (state.isRegistration) {
              NavigationHelper.openMainPage(context);
            }
          }
        },
      ),
    );
  }

  Widget _bodyWidget() {
    return Center(
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 30.w,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _emailPassTextField(_emailController, Icons.email, true),
              SizedBox(
                height: 20.w,
              ),
              _emailPassTextField(_passwordController, Icons.password, false),
              SizedBox(
                height: 70.w,
              ),
              _changedButtonBlock()
            ],
          ),
        ),
      ),
    );
  }

  Widget _changedButtonBlock() {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state is ChangeButtonState) {
          return Column(
            children: [
              _logInOrRegistrationButton(state.isRegistered, context),
              SizedBox(
                height: 60.w,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  state.isRegistered
                      ? LocaleText(UiStrings.doNotHaveAccount,
                          style: TextStyle(fontSize: 14.sp))
                      : const SizedBox(),
                  SizedBox(
                    width: 20.w,
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10.w,
                      ),
                      decoration: BoxDecoration(
                        color: UiTheme.firstTypeBackgroundColor,
                        borderRadius: BorderRadius.circular(5.w),
                      ),
                      height: 26.w,
                      child: Center(
                          child: _switchButton(state.isRegistered, context)))
                ],
              )
            ],
          );
        }
        return const SizedBox();
      },
    );
  }

  Widget _emailPassTextField(TextEditingController _controller, IconData icon, bool isLogin) {
    return TextField(
      style: TextStyle(
        fontSize: 14.sp,
      ),
      cursorColor: UiTheme.firstTypeBackgroundColor,
      controller: _controller,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 10.w, right: 10.w),
          child: Icon(
            icon,
            size: 16.w,
            color: UiTheme.firstTypeBackgroundColor,
          ),
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 20.w,
          vertical: 10.w,
        ),
        filled: true,
        fillColor: UiTheme.secondTypeBackgroundColor,
        hintText: (isLogin) ? UiStrings.email : UiStrings.password,
        hintStyle: TextStyle(
          fontSize: 14.sp
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 2.w,
            color: UiTheme.firstTypeBackgroundColor,
          ),
          borderRadius: BorderRadius.circular(10.w),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            width: 2,
            color: UiTheme.firstTypeBackgroundColor,
          ),
          borderRadius: BorderRadius.circular(10.w),
        ),
      ),
    );
  }

  Widget _logInOrRegistrationButton(bool isRegistered, BuildContext context) {
    return InkWell(
      onTap: () async {
        String email = _emailController.text;
        String password = _passwordController.text;
        if (isRegistered == false) {
          context.read<AuthBloc>().add(RegistrationEvent(email, password));
        } else {
          context.read<AuthBloc>().add(LoginEvent(email, password));
        }
      },
      child: Container(
        width: double.infinity,
        height: 44.w,
        decoration: BoxDecoration(
          color: UiTheme.firstTypeBackgroundColor,
          borderRadius: BorderRadius.circular(10.w),
        ),
        child: Center(
            child: LocaleText(
          (isRegistered)
              ? UiStrings.loginButtonText
              : UiStrings.registrationButtonText,
          style: TextStyle(
            fontSize: 20.sp,
            fontWeight: FontWeight.w700,
          ),
        )),
      ),
    );
  }

  Widget _switchButton(bool isRegistered, BuildContext context) {
    return InkWell(
      onTap: () {
        context.read<AuthBloc>().add(
              ChangeButtonEvent(isRegistered),
            );
      },
      child: (isRegistered)
          ? LocaleText(UiStrings.toRegister, style: TextStyle(fontSize: 14.sp))
          : LocaleText(UiStrings.toLogIn, style: TextStyle(fontSize: 14.sp)),
    );
  }
}
