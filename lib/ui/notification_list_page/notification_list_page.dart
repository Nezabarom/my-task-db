import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_bloc.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_event.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_state.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/constants/ui_theme.dart';
import 'package:my_task_db/models/tasks/task_model.dart';
import 'package:my_task_db/ui/widgets/loading_widget.dart';

class NotificationListPage extends StatelessWidget {
  const NotificationListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.secondTypeBackgroundColor,
      appBar: AppBar(
        toolbarHeight: 50.w,
        backgroundColor: UiTheme.firstTypeBackgroundColor,
        centerTitle: true,
        title: LocaleText(UiStrings.notification,
            style: TextStyle(fontSize: 16.sp)),
        actions: [
          BlocBuilder<NotificationListBloc, NotificationListState>(
              builder: (context, state) {
            if (state is GetNotificationListState) {
              return InkWell(
                onTap: () {
                  context
                      .read<NotificationListBloc>()
                      .add(RemoveNotificationListEvent(state.notificationList));
                },
                child: Container(
                  color: UiTheme.firstTypeBackgroundColor,
                  height: 50.w,
                  width: 100.w,
                  child: Center(
                    child: LocaleText(UiStrings.removeAll,
                        style: TextStyle(fontSize: 14.sp)),
                  ),
                ),
              );
            }
            return const SizedBox();
          })
        ],
      ),
      body: _notificationListBuilder(),
    );
  }

  Widget _notificationListBuilder() {
    return BlocBuilder<NotificationListBloc, NotificationListState>(
        builder: (context, state) {
      if (state is GetNotificationListState) {
        if (state.notificationList.isNotEmpty) {
          return ListView.builder(
              itemCount: state.notificationList.length,
              itemBuilder: (context, index) {
                return _notificationItem(state.notificationList[index], index,
                    state.notificationList, context);
              });
        }
        if (state.notificationList.isEmpty) {
          return Center(
              child: LocaleText(UiStrings.youDontHaveAnyNotificationsYet,
                  style: TextStyle(fontSize: 14.sp)));
        }
      }
      if (state is InitNotificationListState) {
        return const LoadingWidget();
      }
      return const SizedBox();
    });
  }

  Widget _notificationItem(TaskModel model, int index,
      List<TaskModel> notificationList, BuildContext context) {
    DateTime dateDueBy =
        DateTime(1970, 1, 1).add(Duration(seconds: model.dueBy ?? 0));
    return Container(
      margin: EdgeInsets.only(top: 10.w, right: 10.w, left: 10.w),
      padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 25.w),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.blueGrey, borderRadius: BorderRadius.circular(10.w)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: Column(
                children: [
                  Text(model.title ?? '', style: TextStyle(fontSize: 14.sp),),
                  SizedBox(
                    height: 10.w,
                  ),
                  Text('${dateDueBy.month}/${dateDueBy.day}/${dateDueBy.year}',
                  style: TextStyle(fontSize: 14.sp),)
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              )),
              SizedBox(
                width: 10.w,
              ),
              _removeButton(
                  model.intId.toString(), index, notificationList, context)
            ],
          )
        ],
      ),
    );
  }

  Widget _removeButton(String notificationId, int index,
      List<TaskModel> notificationList, BuildContext context) {
    return InkWell(
      onTap: () {
        context.read<NotificationListBloc>().add(RemoveNotificationByIdEvent(
            notificationId, index, notificationList));
      },
      child: Container(
        height: 30.w,
        width: 100.w,
        decoration: BoxDecoration(
            color: UiTheme.darkRedColor,
            borderRadius: BorderRadius.circular(5.w)),
        child: Center(
          child:
              LocaleText(UiStrings.remove, style: TextStyle(fontSize: 14.sp)),
        ),
      ),
    );
  }
}
