import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/bloc/home_bloc/home_bloc.dart';
import 'package:my_task_db/bloc/home_bloc/home_event.dart';
import 'package:my_task_db/bloc/home_bloc/home_state.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_bloc.dart';
import 'package:my_task_db/bloc/notification_list_bloc/notification_list_event.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_bloc.dart';
import 'package:my_task_db/bloc/task_list_bloc/task_list_event.dart';
import 'package:my_task_db/constants/ui_theme.dart';
import 'package:my_task_db/ui/notification_list_page/notification_list_page.dart';
import 'package:my_task_db/ui/settings_page/settings_page.dart';
import 'package:my_task_db/ui/tasks_list_page/tasks_list_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<Widget> _widgetOptions = [
    TasksListPage(),
    const NotificationListPage(),
    SettingsPage()
  ];
  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
      if (state is NavBarTapHomeState) {
        return Scaffold(
          body: PageView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _pageController,
            children: _widgetOptions,
          ),
          bottomNavigationBar: _navBarWidget(state),
        );
      }
      return const SizedBox();
    });
  }

  Widget _navBarWidget(NavBarTapHomeState state) {
    return Theme(
      data: ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      ),
      child: SizedBox(
        height: 50.w,
        child: BottomNavigationBar(
          iconSize: 20.w,
          backgroundColor: UiTheme.firstTypeBackgroundColor,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.list, color: Colors.white),
              activeIcon: Icon(Icons.list, color: UiTheme.darkRedColor),
              label: 'list ot tasks',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications, color: Colors.white),
              activeIcon: Icon(Icons.notifications, color: UiTheme.darkRedColor),
              label: 'priority list',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings, color: Colors.white),
              activeIcon: Icon(Icons.settings, color: UiTheme.darkRedColor),
              label: 'settings',
            ),
          ],
          currentIndex: state.selectedIndex,
          onTap: (int newIndex) {
            if (newIndex == 0) {
              context.read<TaskListBloc>().add(GetTaskListEvent());
            }
            if (newIndex == 1) {
              context
                  .read<NotificationListBloc>()
                  .add(GetNotificationListEvent());
            }
            context.read<HomeBloc>().add(NavBarTapHomeEvent(newIndex));
            _pageController.jumpToPage(newIndex);
          },
        ),
      ),
    );
  }

// Widget _openPageWidget(BuildContext context, int selectedIndex){
//   return IndexedStack(
//     children: _widgetOptions,
//     index: _selectedIndex,
//   );
// }
}
