import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/bloc/task_details_bloc/task_details_bloc.dart';
import 'package:my_task_db/bloc/task_details_bloc/task_details_event.dart';
import 'package:my_task_db/bloc/task_details_bloc/task_details_state.dart';
import 'package:my_task_db/constants/navigation_helper.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/constants/ui_theme.dart';
import 'package:my_task_db/models/tasks/task_model.dart';

class TaskDetailsPage extends StatelessWidget {
  TaskDetailsPage(this.taskModel, {Key? key}) : super(key: key);

  final TaskModel taskModel;
  final List<int> _minuteBeforeList = [10, 20, 30, 40, 50, 60];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[300],
      appBar: AppBar(
        toolbarHeight: 50.w,
        leadingWidth: 50.w,
        leading: InkWell(
            onTap: () {
              NavigationHelper.openMainPage(context);
            },
            child: Padding(
              padding: EdgeInsets.only(left: 15.w),
              child: Icon(Icons.arrow_back,
              size: 20.w,),
            )),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: LocaleText(
          UiStrings.taskDetails,
          style: TextStyle(fontSize: 16.sp),
        ),
      ),
      body: _taskItem(context),
      bottomNavigationBar: SizedBox(
        height: 120.w,
        child: Column(
          children: [
            _deleteOrEditTaskButton(UiStrings.editTask, true, context),
            _deleteOrEditTaskButton(UiStrings.deleteTask, false, context),
          ],
        ),
      ),
    );
  }

  Widget _taskItem(BuildContext context) {
    DateTime dateDueBy =
        DateTime(1970, 1, 1).add(Duration(seconds: taskModel.dueBy!));
    return Container(
      margin: EdgeInsets.all(10.0.w),
      padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 25.w),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.blueGrey, borderRadius: BorderRadius.circular(10.w)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(taskModel.title ?? UiStrings.noData,
              style: TextStyle(fontSize: 14.sp)),
          SizedBox(
            height: 10.w,
          ),
          const Divider(
            color: Colors.white,
            height: 0,
          ),
          SizedBox(
            height: 10.w,
          ),
          Row(
            children: [
              LocaleText(UiStrings.dueBy, style: TextStyle(fontSize: 14.sp)),
              SizedBox(
                width: 20.w,
              ),
              Text('${dateDueBy.month}/${dateDueBy.day}/${dateDueBy.year}',
                  style: TextStyle(fontSize: 14.sp)),
            ],
          ),
          SizedBox(
            height: 10.w,
          ),
          const Divider(color: Colors.white, height: 0),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              LocaleText(UiStrings.priority, style: TextStyle(fontSize: 14.sp)),
              Text(taskModel.priority ?? UiStrings.noData,
                  style: TextStyle(fontSize: 14.sp)),
            ],
          ),
          SizedBox(
            height: 10.w,
          ),
          const Divider(color: Colors.white, height: 0),
          SizedBox(
            height: 10.w,
          ),
          LocaleText(UiStrings.notification, style: TextStyle(fontSize: 14.sp)),
          SizedBox(
            height: 10.w,
          ),
          BlocBuilder<TaskDetailsBloc, TaskDetailsState>(
              builder: (context, state) {
            if (state is InitTaskDetailsState) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Row(
                        children: [
                          _minuteMinusButton(state, context),
                          Container(
                              height: 30.w,
                              width: 30.w,
                              decoration: BoxDecoration(
                                  border: Border.symmetric(
                                      horizontal: BorderSide(
                                          width: 3.w,
                                          color: UiTheme.secondTypeBackgroundColor))),
                              child: Center(
                                  child: Text(
                                      _minuteBeforeList[state.indexMinute]
                                          .toString(),
                                      style: TextStyle(fontSize: 14.sp)))),
                          _minutePlusButton(state, context)
                        ],
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      LocaleText(UiStrings.minuteBefore,
                          style: TextStyle(fontSize: 14.sp)),
                    ],
                  ),
                  _addNotificationButton(state.indexMinute, context)
                ],
              );
            }
            return const SizedBox();
          })
        ],
      ),
    );
  }

  Widget _deleteOrEditTaskButton(
      String textButton, bool editTask, BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (editTask == false) {
          context.read<TaskDetailsBloc>().add(DeleteTaskEvent(taskModel));
          NavigationHelper.openMainPage(context);
        } else {
          NavigationHelper.openEditTaskPage(context, taskModel);
        }
      },
      child: Container(
        height: 50.w,
        margin: EdgeInsets.fromLTRB(10.w, 0, 10, 10.w),
        padding: EdgeInsets.all(10.w),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.w),
            color: (editTask)
                ? UiTheme.firstTypeBackgroundColor
                : UiTheme.darkRedColor),
        child: Center(
            child: LocaleText(
          textButton,
          style: TextStyle(fontSize: 14.sp),
        )),
      ),
    );
  }

  Widget _minuteMinusButton(InitTaskDetailsState state, BuildContext context) {
    return InkWell(
      onTap: () {
        context
            .read<TaskDetailsBloc>()
            .add(DecrementMinuteEvent(state.indexMinute));
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.blueGrey[300],
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5.w),
                bottomLeft: Radius.circular(5.w))),
        height: 30.w,
        width: 30.w,
        child: Center(
          child: Text('-', style: TextStyle(fontSize: 14.sp)),
        ),
      ),
    );
  }

  Widget _minutePlusButton(InitTaskDetailsState state, BuildContext context) {
    return InkWell(
      onTap: () {
        context
            .read<TaskDetailsBloc>()
            .add(IncrementMinuteEvent(state.indexMinute));
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.blueGrey[300],
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(5.w),
                bottomRight: Radius.circular(5.w))),
        height: 30.w,
        width: 30.w,
        child: Center(
          child: Text('+', style: TextStyle(fontSize: 14.sp)),
        ),
      ),
    );
  }

  Widget _addNotificationButton(int indexMinute, BuildContext context) {
    return InkWell(
      onTap: () {
        context.read<TaskDetailsBloc>().add(
            AddNotificationEvent(taskModel, indexMinute, _minuteBeforeList));
      },
      child: Container(
        height: 30.w,
        width: 90.w,
        decoration: BoxDecoration(
            color: Colors.blueGrey[300],
            borderRadius: BorderRadius.circular(5.w)),
        child: Center(
          child: LocaleText(UiStrings.add,
              style: TextStyle(fontSize: 14.sp)),
        ),
      ),
    );
  }
}
