import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:my_task_db/bloc/create_task_bloc/create_task_bloc.dart';
import 'package:my_task_db/bloc/create_task_bloc/create_task_event.dart';
import 'package:my_task_db/bloc/create_task_bloc/create_task_state.dart';
import 'package:my_task_db/constants/navigation_helper.dart';
import 'package:my_task_db/constants/ui_strings.dart';
import 'package:my_task_db/constants/ui_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_task_db/data/repository/network/tasks/tasks_repository_impl.dart';

class CreateTaskPage extends StatefulWidget {
  const CreateTaskPage({Key? key}) : super(key: key);

  @override
  _CreateTaskPageState createState() => _CreateTaskPageState();
}

class _CreateTaskPageState extends State<CreateTaskPage> {
  final TextEditingController _titleController = TextEditingController();
  TimeOfDay? _pickedTime;
  DateTime? _pickedDate;

  @override
  void dispose() {
    _titleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CreateTaskBloc>(
      create: (BuildContext context) =>
          CreateTaskBloc(taskRepository: RepositoryProvider.of<TaskRepository>(context))..add(CreateTaskInitEvent()),
      child: Scaffold(
        backgroundColor: Colors.blueGrey[300],
        appBar: AppBar(
          toolbarHeight: 50.w,
          leadingWidth: 50.w,
          leading: InkWell(
              onTap: () {
                NavigationHelper.openMainPage(context);
              },
              child: Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: Icon(
                  Icons.arrow_back,
                  size: 20.w,
                ),
              )),
          centerTitle: true,
          backgroundColor: Colors.blueGrey,
          title: LocaleText(
            UiStrings.createTask,
            style: TextStyle(fontSize: 16.sp),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(25.w),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LocaleText(
                  UiStrings.titleDescription,
                  style: TextStyle(fontSize: 14.sp),
                ),
                SizedBox(
                  height: 15.w,
                ),
                _titleTextField(),
                SizedBox(
                  height: 15.w,
                ),
                LocaleText(
                  UiStrings.date,
                  style: TextStyle(fontSize: 14.sp),
                ),
                SizedBox(
                  height: 15.w,
                ),
                _datePicker(),
                SizedBox(
                  height: 15.w,
                ),
                LocaleText(
                  UiStrings.time,
                  style: TextStyle(fontSize: 14.sp),
                ),
                SizedBox(
                  height: 15.w,
                ),
                _timePicker(),
                SizedBox(
                  height: 15.w,
                ),
                LocaleText(
                  UiStrings.priority,
                  style: TextStyle(fontSize: 14.sp),
                ),
                SizedBox(
                  height: 15.w,
                ),
                BlocBuilder<CreateTaskBloc, CreateTaskState>(
                    buildWhen: (previous, state) {
                  return state is CreateTaskPrioritySelectorState;
                }, builder: (context, state) {
                  if (state is CreateTaskPrioritySelectorState) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        _priorityButton(0, state.prioritySelectorList[0],
                            UiStrings.low, state, context),
                        _priorityButton(1, state.prioritySelectorList[1],
                            UiStrings.normal, state, context),
                        _priorityButton(2, state.prioritySelectorList[2],
                            UiStrings.high, state, context)
                      ],
                    );
                  }
                  return const SizedBox();
                })
              ],
            ),
          ),
        ),
        bottomNavigationBar: _createButton(),
      ),
    );
  }

  Widget _titleTextField() {
    return TextField(
      controller: _titleController,
      enableInteractiveSelection: true,
      textInputAction: TextInputAction.go,
      maxLines: 3,
      autofocus: false,
      style: TextStyle(
        fontSize: 14.sp,
      ),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
        filled: true,
        fillColor: Colors.blueGrey,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(10.w),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(10.w),
        ),
      ),
    );
  }

  Widget _datePicker() {
    return BlocBuilder<CreateTaskBloc, CreateTaskState>(
        buildWhen: (previous, state) {
      return state is CreateTaskPickDateState;
    }, builder: (context, state) {
      if (state is CreateTaskPickDateState) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              child: Center(
                  child: (_pickedDate != null)
                      ? Text(
                          '${_pickedDate!.month}/${_pickedDate!.day}/${_pickedDate!.year}',
                          style: TextStyle(fontSize: 14.sp),
                        )
                      : LocaleText(
                          UiStrings.notPicked,
                          style: TextStyle(fontSize: 14.sp),
                        )),
              height: 50.w,
              width: 120.w,
              decoration: BoxDecoration(
                  color: UiTheme.firstTypeBackgroundColor,
                  borderRadius: BorderRadius.circular(10)),
            ),
            InkWell(
              onTap: () {
                showDatePicker(
                        context: context,
                        initialDate:
                            DateTime.now().add(const Duration(days: 1)),
                        firstDate: DateTime.now().add(const Duration(days: 1)),
                        lastDate: DateTime(2038, 1, 19))
                    .then((data) {
                  if (data != null) {
                    _pickedDate = data;
                    context
                        .read<CreateTaskBloc>()
                        .add(CreateTaskPickDateEvent(data));
                  }
                });
              },
              child: Container(
                height: 40.w,
                width: 120.w,
                decoration: BoxDecoration(
                    color: UiTheme.firstTypeBackgroundColor,
                    borderRadius: BorderRadius.circular(10.w)),
                child: Center(
                  child: LocaleText(
                    UiStrings.pickDate,
                    style: TextStyle(fontSize: 14.sp),
                  ),
                ),
              ),
            )
          ],
        );
      }
      return const SizedBox();
    });
  }

  Widget _timePicker() {
    return BlocBuilder<CreateTaskBloc, CreateTaskState>(
        buildWhen: (previous, state) {
      return state is CreateTaskPickTimeState;
    }, builder: (context, state) {
      if (state is CreateTaskPickTimeState) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              child: Center(
                  child: (_pickedTime != null)
                      ? Text(
                          (_pickedTime!.minute > 9)
                              ? '${_pickedTime?.hour} : ${_pickedTime?.minute}'
                              : '${_pickedTime?.hour} : 0${_pickedTime?.minute}',
                          style: TextStyle(fontSize: 14.sp),
                        )
                      : LocaleText(
                          UiStrings.notPicked,
                          style: TextStyle(fontSize: 14.sp),
                        )),
              height: 50.w,
              width: 120.w,
              decoration: BoxDecoration(
                  color: UiTheme.firstTypeBackgroundColor,
                  borderRadius: BorderRadius.circular(10.w)),
            ),
            InkWell(
              onTap: () {
                showTimePicker(
                        context: context,
                        initialTime: const TimeOfDay(hour: 09, minute: 00))
                    .then((time) {
                  if (time != null) {
                    _pickedTime = time;
                    context
                        .read<CreateTaskBloc>()
                        .add(CreateTaskPickTimeEvent(time));
                  }
                });
              },
              child: Container(
                height: 40.w,
                width: 120.w,
                decoration: BoxDecoration(
                    color: UiTheme.firstTypeBackgroundColor,
                    borderRadius: BorderRadius.circular(10.w)),
                child: Center(
                  child: LocaleText(
                    UiStrings.pickTime,
                    style: TextStyle(fontSize: 14.sp),
                  ),
                ),
              ),
            )
          ],
        );
      }
      return const SizedBox();
    });
  }

  Widget _priorityButton(int index, bool isSelected, String textButton,
      CreateTaskPrioritySelectorState state, BuildContext context) {
    return InkWell(
      onTap: () {
        context.read<CreateTaskBloc>().add(
            CreateTaskPrioritySelectorEvent(index, state.prioritySelectorList));
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 3.w, color: Colors.blueGrey),
            color: (isSelected) ? Colors.blueGrey : Colors.transparent),
        width: 100.w,
        height: 25.w,
        child: Center(
            child: LocaleText(
          textButton,
          style: TextStyle(fontSize: 14.sp),
        )),
      ),
    );
  }

  Widget _createButton() {
    return BlocConsumer<CreateTaskBloc, CreateTaskState>(
      listener: (context, state) {
        if (state is CreateTaskTapState) {
          NavigationHelper.openMainPage(context);
        }
      },
      buildWhen: (previous, state) {
        return state is CreateTaskPrioritySelectorState;
      },
      builder: (context, state) {
        if (state is CreateTaskPrioritySelectorState) {
          return InkWell(
            onTap: () {
              context.read<CreateTaskBloc>().add(CreateTaskTapEvent(
                  titleController: _titleController.text,
                  pickedDate: _pickedDate,
                  pickedTime: _pickedTime,
                  priorityList: state.prioritySelectorList));
            },
            child: Container(
              margin: EdgeInsets.fromLTRB(10.w, 0, 10, 10.w),
              width: double.infinity,
              height: 50.w,
              decoration: BoxDecoration(
                color: Colors.blueGrey,
                borderRadius: BorderRadius.circular(10.w),
              ),
              child: Center(
                  child: LocaleText(
                UiStrings.createTask,
                style:
                    TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w700),
              )),
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
